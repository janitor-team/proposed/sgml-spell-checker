Source: sgml-spell-checker
Section: text
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: aspell, sgmlspl
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://peter.eisentraut.org/sgml-spell-checker/
Vcs-Browser: https://salsa.debian.org/debian/sgml-spell-checker
Vcs-Git: https://salsa.debian.org/debian/sgml-spell-checker.git

Package: sgml-spell-checker
Architecture: all
Depends: opensp | sp, aspell, sgmlspl, ${misc:Depends}, ${perl:Depends}
Description: spell checker for SGML documents
 This package includes a couple of tools that you can use to
 automatically spell-check your SGML documents. One of the
 advantages of this tool over some other SGML-aware spell
 checkers is that it scans your documents in the form in which
 the SGML parser actually sees it, which means it is not line-based,
 system entities are resolved, marked sections are treated appropriately, etc.
 .
 Also, this tool can be made aware of particular DTDs, in the
 sense that it knows not to spell-check the content of elements
 that do not represent human-language text, such as <programlisting>
 in DocBook. An exclusion list for the DocBook DTD is included, others
 can be added trivially.
 .
 Home page: http://developer.postgresql.org/~petere/sgml-spell-checker/
